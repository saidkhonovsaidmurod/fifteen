import React from "react";
import ReactDOM from "react-dom";
import './index.css'

function Square(props) {
    return (
        <button className={props.value ? "square filled-square" : "square"}
                onClick={props.onClick}>
            {props.value}
        </button>
    )
}

class Board extends React.Component{
    renderSquare(i) {
        return <Square value={this.props.shuffledSquares[i]}
                       onClick={() => this.props.onClick(i)}/>;
    }

    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                    {this.renderSquare(3)}
                </div>
                <div className="board-row">
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                </div>
                <div className="board-row">
                    {this.renderSquare(8)}
                    {this.renderSquare(9)}
                    {this.renderSquare(10)}
                    {this.renderSquare(11)}
                </div>
                <div className="board-row">
                    {this.renderSquare(12)}
                    {this.renderSquare(13)}
                    {this.renderSquare(14)}
                    {this.renderSquare(15)}
                </div>
            </div>
        );
    }
}

class Game extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            squares: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,null],
            shuffledSquares: Array(16),
            win: false,
        };
        this.handleKey = this.handleKey.bind(this);
    }

    componentDidMount() {
        this.shuffleSquares();
        window.addEventListener("keydown", this.handleKey);
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this.handleKey);
    }

    shuffleSquares () {
        this.setState({
            shuffledSquares: shuffle(this.state.squares.slice())
        });
    }

    handleClick(i) {
        let shuffledSquares = this.state.shuffledSquares;
        if (!shuffledSquares[i-4] && (i-4)>=0) this.move(i, i-4);
        else if (!shuffledSquares[i-1] && (i-1)>=0 && (i%4)!=0) this.move(i, i-1);
        else if (!shuffledSquares[i+1] && (i+1)<=15 && ((i+1)%4)!=0) this.move(i, i+1);
        else if(!shuffledSquares[i+4] && (i+4)<=15) this.move(i, i+4);
    }

    handleKey(e) {
        let nullIndex = this.state.shuffledSquares.slice().indexOf(null);
        if (e.keyCode==37 && nullIndex!=15) this.handleClick(nullIndex+1);
        if (e.keyCode==38 && nullIndex<=11) this.handleClick(nullIndex+4);
        if (e.keyCode==39 && (nullIndex%4)!=0) this.handleClick(nullIndex-1);
        if (e.keyCode==40 && nullIndex>=4) this.handleClick(nullIndex-4);
    }

    move (from, to){
        let shuffledSquares = this.state.shuffledSquares.slice();
        shuffledSquares[from] = shuffledSquares[to];
        shuffledSquares[to] = this.state.shuffledSquares[from];
        this.setState({
            shuffledSquares: shuffledSquares,
        });
        this.setState((state) => ({
            win: JSON.stringify(state.shuffledSquares) === JSON.stringify(state.squares)
        }));
    }

    render() {
        const desc = this.state.win ? 'Congrats! You Won'
                        : 'Arrange numbers in increasing order';
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        shuffledSquares={this.state.shuffledSquares}
                        onClick={(i)=>this.handleClick(i)} />
                </div>
                <div className="game-info">
                    <button onClick={() => this.shuffleSquares()}>Reshuffle</button>
                </div>
                <h1>{desc}</h1>
            </div>
        )
    }
}

function shuffle(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

ReactDOM.render(<Game />, document.getElementById('root'))